<?php

/**
 * Views field handler for basic drupalorg_crosssite views_handler_field_entity.
 */
class drupalorg_crosssite_uid_handler_field extends views_handler_field_entity {
  function query() {}

  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  function render($values) {
    if ($entity = $this->get_value($values)) {
      return drupalorg_crosssite_get_drupalorg_uid($entity);
    }
  }
}
