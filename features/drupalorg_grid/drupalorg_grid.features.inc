<?php
/**
 * @file
 * drupalorg_grid.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function drupalorg_grid_image_default_styles() {
  $styles = array();

  // Exported image style: grid-1.
  $styles['grid-1'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 60,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-1',
  );

  // Exported image style: grid-1-2x.
  $styles['grid-1-2x'] = array(
    'label' => 'grid-1-2x',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 120,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-10.
  $styles['grid-10'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 700,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-10',
  );

  // Exported image style: grid-10-2x.
  $styles['grid-10-2x'] = array(
    'label' => 'grid-10-2x',
    'effects' => array(
      10 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1560,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-11.
  $styles['grid-11'] = array(
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 860,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-11',
  );

  // Exported image style: grid-11-2x.
  $styles['grid-11-2x'] = array(
    'label' => 'grid-11-2x',
    'effects' => array(
      11 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1720,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-12.
  $styles['grid-12'] = array(
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 940,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-12',
  );

  // Exported image style: grid-12-2x.
  $styles['grid-12-2x'] = array(
    'label' => 'grid-12-2x',
    'effects' => array(
      12 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1880,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-2.
  $styles['grid-2'] = array(
    'effects' => array(
      5 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 140,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-2',
  );

  // Exported image style: grid-2-2x.
  $styles['grid-2-2x'] = array(
    'label' => 'grid-2-2x',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 280,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-2-2x-square.
  $styles['grid-2-2x-square'] = array(
    'label' => 'grid-2-2x-square',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 280,
          'height' => 280,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-3.
  $styles['grid-3'] = array(
    'effects' => array(
      6 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 220,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-3',
  );

  // Exported image style: grid-3-2x.
  $styles['grid-3-2x'] = array(
    'label' => 'grid-3-2x',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 440,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-4.
  $styles['grid-4'] = array(
    'effects' => array(
      7 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-4',
  );

  // Exported image style: grid-4-2x.
  $styles['grid-4-2x'] = array(
    'label' => 'grid-4-2x',
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 600,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-5.
  $styles['grid-5'] = array(
    'effects' => array(
      8 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 380,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-5',
  );

  // Exported image style: grid-5-2x.
  $styles['grid-5-2x'] = array(
    'label' => 'grid-5-2x',
    'effects' => array(
      5 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 760,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-6.
  $styles['grid-6'] = array(
    'effects' => array(
      9 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 460,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-6',
  );

  // Exported image style: grid-6-2x.
  $styles['grid-6-2x'] = array(
    'label' => 'grid-6-2x',
    'effects' => array(
      6 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 920,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-7.
  $styles['grid-7'] = array(
    'effects' => array(
      10 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 540,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-7',
  );

  // Exported image style: grid-7-2x.
  $styles['grid-7-2x'] = array(
    'label' => 'grid-7-2x',
    'effects' => array(
      7 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1080,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-8.
  $styles['grid-8'] = array(
    'effects' => array(
      11 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 620,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-8',
  );

  // Exported image style: grid-8-2x.
  $styles['grid-8-2x'] = array(
    'label' => 'grid-8-2x',
    'effects' => array(
      8 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1240,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: grid-9.
  $styles['grid-9'] = array(
    'effects' => array(
      12 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 700,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'grid-9',
  );

  // Exported image style: grid-9-2x.
  $styles['grid-9-2x'] = array(
    'label' => 'grid-9-2x',
    'effects' => array(
      9 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1400,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
