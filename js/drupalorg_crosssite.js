(function ($) {

  // Enable Timago only if a user's browser has localStorage support.
  // They must be able to double click it to enable/disable at will.
  var localStorage = 'localStorage' in window && typeof window.localStorage !== 'undefined' && window['localStorage'] !== null;
  var timeagoEnabled = localStorage ? (typeof window.localStorage['drupalorgCommentTimeago'] !== 'undefined' && window.localStorage['drupalorgCommentTimeago'] === 'false' ? false : true) : false;

  // Callback for toggling timestamps between set dates and relative time.
  function toggleTimes () {
    var $element = $(this);

    // Retrieve the original created date.
    var createdDate = $element.attr('data-created-date');
    if (!createdDate) {
      createdDate = $element.text();
      $element.attr('data-created-date', createdDate);
    }

    // Determine which title to use.
    var title = timeagoEnabled ? Drupal.t('!created_date (click to toggle)', {
      '!created_date': createdDate
    }) : Drupal.t('Click to show time ago');

    // Configure the timestamp accordingly.
    $element
      .attr('title', title)
      .timeago(timeagoEnabled ? undefined : 'dispose')
      .text(!timeagoEnabled ? createdDate : undefined);
  }

  Drupal.behaviors.drupalorgCrosssite = {
    attach: function (context) {
      $(context).find('.comments').once('timeago', function () {
        var $times = $(this).find('.comment .submitted time');
        $times
          // Initialize all timestamps.
          .each(toggleTimes)
          // Click to toggle all timestamps at the same time.
          .bind('click', function () {
            timeagoEnabled = !timeagoEnabled;
            if (localStorage) {
              window.localStorage['drupalorgCommentTimeago'] = timeagoEnabled;
            }
            $times.each(toggleTimes);
          });
      });
    }
  };

  Drupal.behaviors.drupalorgCrosssiteCookieConsent = {
    attach: function (context) {
      var gdpr, gdprEdit;

      if (!(context instanceof HTMLDocument)) {
        return;
      }

      if (Drupal.drupalorgCrosssite.doNotTrack()) {
        return;
      }

      // Above header.
      if ((gdpr = context.getElementById('drupalorg-crosssite-gdpr')) && !gdpr.classList.contains('processed')) {
        gdpr.classList.add('processed');

        if (Drupal.drupalorgCrosssite.consentStatus() === null) {
          gdpr.classList.add('prompt');
          gdpr.addEventListener('click', function (e) {
            if (e.target.classList.contains('yes')) {
              Drupal.drupalorgCrosssite.setConsent(true);
            }
            else if (e.target.classList.contains('no')) {
              Drupal.drupalorgCrosssite.setConsent(false);
            }
            else {
              return;
            }
            gdpr.classList.remove('prompt');
          });
        }
      }
      // Edit on /terms.
      if ((gdprEdit = context.getElementById('drupalorg-crosssite-gdpr-edit')) && !gdprEdit.classList.contains('processed')) {
        gdprEdit.classList.add('processed');

        if (Drupal.drupalorgCrosssite.consentStatus() !== null) {
          gdprEdit.classList.add('prompt');
          gdprEdit.classList.add(Drupal.drupalorgCrosssite.consentStatus());
          gdprEdit.addEventListener('click', function (e) {
            if (e.target.classList.contains('yes')) {
              Drupal.drupalorgCrosssite.setConsent(true);
            }
            else if (e.target.classList.contains('no')) {
              Drupal.drupalorgCrosssite.setConsent(false);
            }
            else {
              return;
            }
            gdprEdit.classList.remove('optin', 'optout');
            gdprEdit.classList.add(Drupal.drupalorgCrosssite.consentStatus());
          });
        }
      }
    }
  };

  Drupal.drupalorgCrosssite = {
    doNotTrack: function () {
      return navigator.doNotTrack == "yes" || navigator.doNotTrack == "1" || navigator.msDoNotTrack == "1" || window.doNotTrack == "1";
    },
    consentStatus: function () {
      return $.cookie('cookieConsent');
    },
    canTrack: function () {
      return !this.doNotTrack() && (!Drupal.settings.drupalorgCrosssiteConsentNeeded || this.consentStatus() === 'optin');
    },
    setConsent: function (consent) {
      $.cookie('cookieConsent', consent ? 'optin' : 'optout', {
        expires: consent ? 365 : 0,
        path: '/',
        domain: document.domain.replace(/^.*((\.[^.]+){2})$/, '$1'),
        secure: true
      });
    }
  };

})(jQuery)
