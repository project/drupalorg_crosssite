(function(document, Drupal) {
  /**
   * Auto-submit form.
   */
  Drupal.behaviors.drupalorg_login = {
    attach: function () {
      var form = document.getElementById("openid-connect-login-form");
      form.querySelector(".form-submit").style.display = "none";
      form.submit();
    }
  };
})(document, Drupal);
