<?php

//function drupalorg_corsssite_views_data_alter()
function drupalorg_crosssite_views_data()
{
  $data = array();

  $data['views_entity_user']['drupalorg_uid'] = array(
    'field' => array(
      'title' => t('Drupal.org uid'),
      'help' => t('Drupal.org uid'),
      'handler' => 'drupalorg_crosssite_uid_handler_field',
    ),
  );

  return $data;
}
