<?php

$plugin = [
  'title' => t('Cookie consent control'),
  'single' => TRUE,
  'category' => t('Drupal.org'),
  'render callback' => 'drupalorg_crosssite_consent_control',
];

/**
 * Form for revoking or allowing cookie consent.
 */
function drupalorg_crosssite_consent_control($subtype, $conf, $args, $context = NULL) {
  $block = new stdClass();

  // Logged in users have consented as part of ToS. Only show if user has opted
  // in or out.
  if (user_is_anonymous() && isset($_COOKIE['cookieConsent']) && ($copy = variable_get('drupalorg_crosssite_gdpr_copy'))) {
    $block->content = '<div id="drupalorg-crosssite-gdpr-edit">' . filter_xss($copy['message']) . '<br><button class="yes">' . $copy['yes'] . '</button><button class="no">' . $copy['no'] . '</button><br>' . $copy['status'] . '</div>';
  }

  return $block;
}
